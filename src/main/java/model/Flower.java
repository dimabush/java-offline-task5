package model;

public class Flower {
  private long id;
  private String name;
  private int length;
  private double price;
  private int amount;

  public Flower(long id, String name, int length, double price, int amount) {
    this.id = id;
    this.name = name;
    this.length = length;
    this.price = price;
    this.amount = amount;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    return "Flower{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", length=" + length +
        ", price=" + price +
        ", amount=" + amount +
        '}';
  }
}

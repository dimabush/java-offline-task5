import model.Flower;
import model.Flowerpot;

public class Main {
  public static void main(String[] args) {

    Flower rose = new Flower(1, "Red rose", 70, 30.50, 40);
    Flower chamomile = new Flower(2, "Сhamomile", 50, 20.0, 50);
    Flower lilyOfTheValley = new Flower(
        3, "Lily of the valley", 60, 25.50, 20
    );

    Flowerpot cactus = new Flowerpot(1, "Cactus", 80, 10);
    Flowerpot violet = new Flowerpot(2, "Violet", 90, 15);
  }
}
